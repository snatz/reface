# Requirements

* OpenCV 3.1+
* CMake 3.9+

# Usage

Build using CMake, then execute

```
./reface image_to_reface
```

The Haar face classifier path must be ``../data/haarcascade_frontalface_alt.xml``.

The image saved is in the current directory.
