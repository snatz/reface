#include <opencv2/imgproc.hpp>
#include <iostream>
#include <opencv/cv.hpp>
#include "FaceDetection.h"

using namespace std;
using namespace cv;

FaceDetection::FaceDetection() = default;

/**
 * Detect faces in a given image.
 * @param t_mat The image to detect faces from.
 * @return A vector of Rect instances framing the faces.
 */
vector<Rect> FaceDetection::detectFaces(Mat t_mat) {
    CascadeClassifier faceCascade;

    if (!faceCascade.load("../data/haarcascade_frontalface_alt.xml")) {
        cerr << "Error loading the face cascade classifier." << endl;
        return vector<Rect>();
    }

    vector<Rect> faces;
    Mat grayImage;

    // Convert to gray.
    cvtColor(t_mat, grayImage, CV_BGR2GRAY);

    // Normalize brightness and increase contrast of the gray image.
    equalizeHist(grayImage, grayImage);

    // Detect faces.
    faceCascade.detectMultiScale(grayImage, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30));

    return faces;
}
