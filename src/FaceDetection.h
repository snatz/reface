#ifndef REFACE_FACEDETECTION_H
#define REFACE_FACEDETECTION_H

#include <opencv2/core/mat.hpp>
#include <opencv2/objdetect/objdetect.hpp>

using namespace cv;

class FaceDetection {
public:
    static std::vector<Rect> detectFaces(cv::Mat t_mat);

private:
    FaceDetection();
};


#endif //REFACE_FACEDETECTION_H
