#include <dirent.h>
#include <iostream>
#include <random>
#include <opencv2/highgui/highgui.hpp>
#include "ImageBank.h"

using namespace std;
using namespace cv;

ImageBank::ImageBank() = default;

/**
 * Load a specific image from the images bank based on a index.
 * @param t_index The index of the image to choose.
 * @return The image laoded.
 */
Mat ImageBank::loadSpecific(int t_index) {
    string path = "../images/";
    Mat image = imread(path.append(pickImage(t_index)), CV_LOAD_IMAGE_COLOR);

    if (!image.data) {
        cerr << "Couldn't open or find the image." << t_index << endl;
        return Mat();
    }

    return image;
}

/**
 * Load a random image from the images bank.
 * @return The image loaded.
 */
Mat ImageBank::load() {
    // Generate a pseudo-random number between 0 and the number of images minus 1.
    random_device rd;
    mt19937 mt(rd());
    uniform_int_distribution<int> dist(0, countImages() - 1);

    return loadSpecific(dist(mt));
}

/**
 * Pick the file name of an image in the images directory.
 * @param t_index Index of the image to choose.
 * @return The file name of the image according to the index.
 */
string ImageBank::pickImage(int t_index) {
    DIR *dp;
    int imageCount = 0;
    struct dirent *ep;
    string filename;
    string jpgext = string(".jpg");
    string pngext = string(".png");

    dp = opendir("../images/");

    if (dp != nullptr) {
        while ((ep = readdir(dp))) {
            filename = string(ep->d_name);

            if (ImageBank::hasEnding(filename, jpgext) || ImageBank::hasEnding(filename, pngext)) {
                if (imageCount == t_index)
                    break;

                imageCount++;
            }
        }

        (void) closedir(dp);
    } else {
        cerr << "Couldn't open the images directory." << endl;
        return string("");
    }

    return filename;
}

/**
 * Count the number of jpg and png files in the images directory.
 * @return The number of images.
 */
int ImageBank::countImages() {
    DIR *dp;
    int imageCount = 0;
    struct dirent *ep;
    string filename;
    string jpgext = string(".jpg");
    string pngext = string(".png");

    dp = opendir("../images/");

    if (dp != nullptr) {
        while ((ep = readdir(dp))) {
            filename = string(ep->d_name);

            if (ImageBank::hasEnding(filename, jpgext) || ImageBank::hasEnding(filename, pngext)) {
                imageCount++;
            }
        }

        (void) closedir(dp);
    } else {
        cerr << "Couldn't open the images directory." << endl;
        return -1;
    }

    return imageCount;
}


/**
 * Check if a string ends with another.
 * @param fullString String to check on.
 * @param ending String to check with.
 * @return true or false based on the end of fullString.
 */
bool ImageBank::hasEnding(const std::string &fullString, const std::string &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}
