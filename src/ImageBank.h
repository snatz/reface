#ifndef REFACE_IMAGEBANK_H
#define REFACE_IMAGEBANK_H

#include <opencv2/core/mat.hpp>

class ImageBank {
public:
    static cv::Mat load();
    static cv::Mat loadSpecific(int t_index);

private:
    ImageBank();

    static int countImages();
    static bool hasEnding(const std::string &fullString, const std::string &ending);
    static std::string pickImage(int t_index);
};


#endif //REFACE_IMAGEBANK_H
