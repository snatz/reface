#include <iostream>
#include <opencv2/imgproc.hpp>
#include "Reface.h"

using namespace std;
using namespace cv;

Reface::Reface(Mat t_source, vector<Rect> t_faces) : m_source(move(t_source)), m_faces(move(t_faces)) {

}

/**
 * Apply the reface operation on the source image.
 * @param t_newFaces A vector of Mat instances with the funnier faces.
 * @return The source image overwritten with the new faces.
 */
Mat Reface::reface(vector<Mat> *t_newFaces) {
    Mat t_newFace, croppedFace;
    Rect m_faceRect;

    for (unsigned long i = 0; i < m_faces.size(); i++) {
        // Get new face.
        t_newFace = t_newFaces->at(i);

        // Get the source face to replace.
        m_faceRect = m_faces.at(i);

        // Resize the new face.
        resize(t_newFace, t_newFace, m_faceRect.size(), 0, 0, CV_INTER_CUBIC);

        // Copy the new face to the image source.
        t_newFace.copyTo(m_source(m_faceRect));
    }

    return m_source;
}
