#ifndef REFACE_REFACE_H
#define REFACE_REFACE_H

#include <opencv2/core/mat.hpp>

using namespace cv;

class Reface {
public:
    Reface(Mat t_source, std::vector<Rect> t_faces);

    Mat reface(std::vector<Mat> *t_newFaces);

private:
    Mat m_source;
    std::vector<Rect> m_faces;
};


#endif //REFACE_REFACE_H
