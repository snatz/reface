#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgcodecs/imgcodecs_c.h>
#include "FaceDetection.h"
#include "Reface.h"
#include "ImageBank.h"

using namespace std;
using namespace cv;

int main(int argc, char** argv) {
    if (argc != 2) {
        cout << "Usage: " << argv[0] << " image_to_reface" << endl;
        return 0;
    }

    // Load the input image.
    Mat image = imread(argv[1], CV_LOAD_IMAGE_COLOR);

    if (!image.data) {
        cerr << "Couldn't open or find the image." << endl;
        return 0;
    }

    // Detect the faces in the image.
    vector<Rect> faces = FaceDetection::detectFaces(image);

    // Instantiate Reface.
    Reface *reface = new Reface(image, faces);

    // Load the images from the bank.
    auto *funnierImages = new vector<Mat>();

    for (int i = 0; i < faces.size(); i++) {
        funnierImages->push_back(ImageBank::load());
    }

    // Replace the faces with random funnier face images.
    Mat refaced = reface->reface(funnierImages);

    // Save refaced image.
    imwrite("refaced.jpg", refaced);

    return 0;
}
